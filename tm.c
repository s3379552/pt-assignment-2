/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
/**************************************************************************
* main() - this is the entry point into your program. You need to follow 
* the steps in the comments provided as these are the steps required to 
* run your program. Please ensure that you use all function prototypes and 
* datatype declarations provided as not doing so will result in a 
* substantial mark penalty as well as missing out on some important learning
* opportunities.
**************************************************************************/
#include "tm.h"

int main(int argc, char** argv)
{
   FILE *coinFP;
   FILE *stockFP;
   char * coinsarray;
   char * stockarray;
   int coin_size;
   int stock_size;
   int fread_result;
   tm_type tm;
   tm_type_ptr tmptr;
   BOOLEAN quit = FALSE;
   unsigned menuChoice;
   int load_data_check;
   tmptr = &tm;
    /* check command line arguments */
   if (argc != 3)
   {
      fprintf(stderr, "Program %s takes in 2 command ", argv[0]);
      fprintf(stderr, "line arguments only.\n");
      return EXIT_FAILURE;
   }
   coinFP = fopen(argv[2], "r");
   stockFP = fopen(argv[1], "r");
   if(coinFP == NULL || stockFP == NULL)
   {   
      fprintf(stderr, "Error reading files.\n");
      return EXIT_FAILURE;
   }

    /* initialise data structures */
      system_init(tmptr);
    /* load data */
   
   /*loading coins array */
   
   /* get size of coins file */
   fseek(coinFP, 0, SEEK_END);
   coin_size = ftell(coinFP);
   rewind(coinFP);
   /* allocate memory to coinsarray */
   coinsarray = (char*)malloc (sizeof(char)*coin_size);
   if(coinsarray == NULL)
   {
      fprintf(stderr, "Error allocating memory.\n");
      return EXIT_FAILURE;
   }
   /* read file to array */
   fread_result = fread(coinsarray,1,coin_size,coinFP);
   if(fread_result != coin_size)
   {
      fprintf(stderr, "Error reading coins file.\n");
      return EXIT_FAILURE;
   }
   
   /* loading stock array */
   
   /* get size of stock file */ 
   fseek(stockFP, 0, SEEK_END);
   stock_size = ftell(stockFP);
   rewind(stockFP);
   
   /* allocate memory to stockarray */
   stockarray = (char*)malloc (sizeof(char)*stock_size);
   if(stockarray == NULL)
   {
      fprintf(stderr, "Error allocating memory.\n");
      return EXIT_FAILURE;
   }
   
   /* read stock file to stockarray */
   fread_result = fread(stockarray,1,stock_size,stockFP);
   if(fread_result != stock_size)
   {
      fprintf(stderr, "Error reading stock file.\n");
      return EXIT_FAILURE;
   }
   
   /* load stock and coins data into ticketing machine */
   load_data_check = load_data(tmptr, stockarray, coinsarray);
   if(!load_data_check)
   {
      fprintf(stderr, "Incorrect format in files passed to program.\n");
      return EXIT_FAILURE;
   }

   /* close coin and stock files */
   fclose(coinFP);
   fclose(stockFP);
   free(coinsarray);
   free(stockarray);

    while(!quit)
    {

        /* display menu */
         printf("------------------------\n");
         printf("Main Menu:\n");
         printf("------------------------\n");
         printf("1)\tPurchase Ticket\n");
         printf("2)\tDisplay Tickets\n");
         printf("3)\tSave and Exit\n");
         printf("------------------------\n");
         printf("Administrator-Only Menu:\n");
         printf("------------------------\n");
         printf("4)\tAdd Ticket\n");
         printf("5)\tRemove Ticket\n");
         printf("6)\tDisplay Coins\n");
         printf("7)\tRestock Tickets\n");
         printf("8)\tRestock Coins\n");
         printf("9)\tAbort\n");
         printf("\n");
         printf("------------------------\n");
         printf("Select your option (1-9): ");
        /* perform menu choice */
         if(!read_int_input(&menuChoice, MAX_MENU_LENGTH, MAX_MENU_SIZE))
         {
            printf("\n");
            continue;
         }
         switch(menuChoice)
         {
            case 1:
            {
               printf("\n");
               purchase_ticket(tmptr);
               printf("\n");
               break;
            }
            case 2:
            {
               printf("\n");
               display_tickets(tmptr);
               printf("\n");
               break;
            }
            case 3:
            {  
               printf("\n");
               save_data(tmptr, argv[1], argv[2]);
               printf("All data successfully saved.\n");
               quit = TRUE;
               printf("\n");
               break;
            }
            case 4:
            {
               printf("\n");
               add_ticket(tmptr);
               printf("\n");
               break;
            }
            case 5:
            {
               printf("\n");
               delete_ticket(tmptr);
               printf("\n");
               break;
            }
            case 6:
            {
               printf("\n");
               display_coins(tmptr);
               printf("\n");
               break;
            }
            case 7:
            {
               printf("\n");
               restock_tickets(tmptr);
               printf("\n");
               break;
            }
            case 8:
            {
               printf("\n");
               restock_coins(tmptr);
               printf("\n");
               break;
            }
            case 9:
            {
               printf("\n");
               printf("ABORTING PROGRAM.\n");
               quit = TRUE;
               printf("\n");
               break;
            }
            default :
            {
               printf("\n");
               printf("Please enter a valid menu input.\n");
               printf("\n");
            }
         }
    }

    /* free memory */
   system_free(tmptr);
    /* leave program */
   return EXIT_SUCCESS;
}


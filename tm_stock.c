/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm_stock.h"

/* add functions here to manipulate the stock list */

/* Function for initialising the stock linked list */
void stock_init(tm_type *tm)
   {
      /* Create list and initial null node */
      struct stock_node * head_node = NULL;
      struct stock_list * st_list = NULL;
      head_node = malloc(sizeof(struct stock_node));
      st_list = malloc(sizeof(struct stock_list));
      
      /* Initialise the list and the first node */
      head_node->next_node = NULL;
      st_list->head_stock = NULL;
      st_list->num_stock_items = 1;
      
      /* Set created list to be the stock list of the ticketing machine*/
      tm->stock = st_list;
      return;
   }

/* Function for creating a new ticket node from the paramaters passed.*/
int new_ticket(tm_type *tm, char * new_ticket_name, 
   char new_ticket_type, char * new_ticket_zone, unsigned new_ticket_price, 
   unsigned new_stock_level)
   {
      /* Create a new node to be filled with the paramater data
 *       Start node pointer for the loop at the start of the stock list */
      struct stock_node * new_node = NULL;
      struct stock_node * current_node = tm->stock->head_stock;
      struct stock_node * previous_node = NULL;
      /* head_flag indicates that the current node position is at the head
 *       of the stock list. Used for inserting new nodes at this head 
 *       position*/
      int head_flag = TRUE;
      new_node = malloc(sizeof(struct stock_node));
      assert(new_node != NULL);
      new_node->data = malloc(sizeof(struct stock_data));
      
      /* Fill the new node with the ticket information passed to the function*/
      strcpy(new_node->data->ticket_name, new_ticket_name);
      new_node->data->ticket_type = new_ticket_type;
      strcpy(new_node->data->ticket_zone, new_ticket_zone);
      new_node->data->ticket_price = new_ticket_price;
      new_node->data->stock_level = new_stock_level;
      
      /* Reallocate the stock list's memory size to be able to include
 *       the new node */
      tm->stock = realloc(tm->stock, sizeof(tm->stock) + 
         sizeof(struct stock_node));
      assert(tm->stock != NULL);
      
      /* Loop through the current stock list to find the correct position
 *       to insert this new node it (alphabetical order) */
      while(current_node != NULL)
      { 
         if(strcmp(new_ticket_name, current_node->data->ticket_name) < 0)
            break;
         head_flag = FALSE;  
         previous_node = current_node;
         current_node = current_node->next_node;
      }
      /*if(memcmp(tm->stock->head_stock, current_node, 
         sizeof(struct stock_node)) == 0 )*/

      /* If the needed node position is the head of the stock list
 *       call the new_head function */
      if(head_flag == TRUE)
      {
         new_head(&tm->stock->head_stock, new_node);
         return TRUE;
      }
      
      /* otherwise, insert the new node behind the current node position */
      else
      {
         new_node->next_node = current_node;
         previous_node->next_node = new_node;
         tm->stock->num_stock_items++;
         return TRUE;
      }
   }

/* Function for reassigning the head node pointer in the stock list */
void new_head(struct stock_node **head_ptr, struct stock_node * new_head)
   {
      /* Inserts new node into stock list head, changes the head pointer
 *       accordingly */
      new_head->next_node = *head_ptr;
      *head_ptr = new_head;
   }

/* Function for returning a ticket node with the requested name, type 
 * and zone */
struct  stock_data * find_ticket(tm_type *tm, char * ticket_name, 
   char * ticket_type, char * ticket_zone)
   {
      /* Loop for checking if each node matches all 3 passed parameters */
      struct stock_node * current_node = tm->stock->head_stock;
      while(current_node != NULL)
      {
         if(strcmp(current_node->data->ticket_name, ticket_name) == 0)
         {
            if(current_node->data->ticket_type == *ticket_type)
            {
               if(strcmp(current_node->data->ticket_zone, ticket_zone) == 0)
               {
                     /*If all 3 match, return the node */
                     return current_node->data;
               }
            }
         }
         current_node = current_node->next_node;
      }
      /* If a correct node cannot be found, return null */
      return NULL;
   }

/* Function for removing a ticket node from the list */
int remove_ticket(tm_type * tm, char * ticket_name, char * ticket_type, 
   char * ticket_zone)
{
   struct stock_node * current_node = tm->stock->head_stock;
   struct stock_node * previous_node = NULL;
   while(current_node != NULL)
      {
         if(strcmp(current_node->data->ticket_name, ticket_name) == 0)
         {
            if(current_node->data->ticket_type == *ticket_type)
            {
               if(strcmp(current_node->data->ticket_zone, ticket_zone) == 0)
               {   
                  /* If the name, type and zone match AND the node is not the
 *                   head node, remove the node from the list and free its
 *                   memory */
                  if(previous_node != NULL)
                  {
                     previous_node->next_node = current_node->next_node;
                     free(current_node);
                  }
                  /* If it is the head node, change the head node pointer and
 *                   free the node's memory */
                  else
                  {
                     tm->stock->head_stock = current_node->next_node;
                     free(current_node);
                  }
                  /* Once the node is removed, change the number of nodes
 *                   counter and return True */
                     tm->stock->num_stock_items--;
                  return TRUE;
               }   
            }
         }
         previous_node = current_node;
         current_node = current_node->next_node;
      }
   /* If a node cannot be found with the requested name, zone and type return
 *    False */   
   return FALSE;
}

/* Function for changing all tickets stock level to the default level */
void resupply_tickets(tm_type * tm)
   {
      struct stock_node * current_node = tm->stock->head_stock;
      
      /* Go through every node in the stock list and change the stock
 *       level to the default constant value */
      while(current_node != NULL)
      {
         current_node->data->stock_level = DEFAULT_STOCK_LEVEL;
         current_node = current_node->next_node;
      }
   }

/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#ifndef TM_OPTIONS_H
#define TM_OPTIONS_H

#include "tm.h"
/* function prototypes you need to implement to fulfil the required options
 */
void purchase_ticket(tm_type_ptr tm);
/* Function for purchasing a ticket from the ticketing machine */
/* Function first uses get_ticket_input function to get ticket details from
 * the user. Find_ticket() is then used to search for the corresponding
 * ticket requested from the user. If null is returned when finding the
 * ticket, the requested ticket doesn't exist and the function exits.
 * The function also exits if the requested ticket's stock is 0.
 * Otherwise, the price is displayed to the user, and the coin input loop
 * is entered. For each coin that is entered, that coin's counter is increased
 * by one. This loop is broken when enough coins are entered to pay for the
 * ticket. These entered coins are then added to the coins array stock.
 * If no change is needed, then the ticket is purchased and the function
 * exits. Otherwise, the change loop is entered.
 * The change loop checks for coins in descending value, checking if there
 * are both coins of that type available and that coin can be given as change.
 * Each successful change coin has its counter incremented by one and is
 * printed to the user, as well as deducting its value from the change needed
 * count. Once the change needed reaches zero, the function exits and that
 * ticket's stock level is decreased by one. If the correct change cannot
 * be given, then all coins entered by the user are removed from the machine
 * and any previous coins given as change are resupplyed to the machine, then
 * the function exits.*/

void display_tickets(tm_type_ptr tm);
/* Function for displaying all tickets in the machine
 * Loops through stock list and prints the name, type, zone, price and quantity
 * for each ticket. */

void add_ticket(tm_type_ptr tm);
/* Function for adding a new ticket to the ticketing machine.
 * get_ticket_input is first called to get the ticket name, type and zone
 * if this returns false, the function exits. The price of the new ticket
 * is then taken from the user with read_int_input. If this returns False,
 * the function exits. Otherwise, this new ticket is added to the machine. */

void delete_ticket(tm_type_ptr tm);
/* Function for deleting a ticket from the ticketing machine.
 * get_ticke_input is first called to get the ticket details.
 * delete ticket is then used to delete the ticket from the machine. */

void display_coins(tm_type_ptr tm);
/* Function for displaying all coins in the coins array
 * Loops through coins array and displays the coin type, quantity and total
 * value for each coin. The total value of all coins in the machine is also
 * displayed. */

void restock_tickets(tm_type_ptr tm);
/* Function for restocking all tickets to the default level.*/
void restock_coins(tm_type_ptr tm);
/* Function for restocking all coins to the default level */

void save_data(tm_type_ptr tm, char * stockfile, char * coinsfile);
/* Fuction to save ticketing machine data to command line files.
 * Creates two file pointers for each file name passed into the function.
 * For the Stock file, the stock list is looped through, with each piece of
 * data written to the stock file. If the next node is not null, newline
 * character is printed for the next node.
 * For the coins file, each coin type and value is printed, and if there is 
 * another coin after the current one, the newline character is printed.
 * Both file pointers are then closed. */
#endif

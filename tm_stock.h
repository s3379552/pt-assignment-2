/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#ifndef TM_STOCK_H
#define TM_STOCK_H
#include "tm.h"

struct stock_data 
{
    char ticket_name[TICKET_NAME_LEN+1];
    char ticket_type;
    char ticket_zone[TICKET_ZONE_LEN+1];
    unsigned int ticket_price;
    unsigned int stock_level;
};
typedef struct stock_node 
{
    struct stock_data * data;
    struct stock_node * next_node;
} stock_node;

struct stock_list
{
    stock_node * head_stock;
    unsigned int num_stock_items;
};
void stock_init(tm_type_ptr tm);
/* Function for initialising the stock linked list.
 * Creates a stock node list (st_list), initialised to null
 * Creates a head node initialised to null
 * Allocates memory to both
 * Points head of stock list to the null head node, sets total node count to 1
 * Ties created stock list to the ticket machine*/
int new_ticket(tm_type_ptr tm, char * new_ticket_name, 
   char new_ticket_type, char * new_ticket_zone, unsigned new_ticket_price, 
   unsigned new_stock_level);
/* Creates new node for the new ticket to be stored in, and sets current node
 * pointer starting at the head node of stock list
 * Allocates memory for the new ticket node and its data
 * Copies over each element of stock data from the values passed to the 
 * function
 * Reallocte memory in the stock list with space for the new ticket
 * If the current node pointer is NULL, the head of the list is empty, and
 * the new node is placed in the head position using the new_head function.
 * If the new node's ticket name is "smaller" than the current head node's
 * ticket name, this same new_head function is called. 
 * Otherwise, the loop for placing the ticket into the stock list is entered.
 * This loop compares the new node's ticket name against the current node's
 * ticket name. If the new node's name is alphabetically "smaller" than the
 * current node, the loop is broken out of. Otherwise, keep stepping through
 * the stock list and comparing the ticket names against the new node
 * Once the correct position is found, the node is inserted into the list behind
 * the current node pointed to in the list, and the relevant next node links 
 * are changed. The number of stock items is also increased by one in the stock
 * list*/

void new_head(struct stock_node **head_ptr, struct stock_node * new_head);
/* Function for reassigning the head node of the stock list.
 * This function assign's the new head's (passed as a parameter) next node to 
 * be the current head node, then changes the passed in stock list head node
 * pointer to be the new head node.*/

struct  stock_data * find_ticket(tm_type_ptr tm, char * ticket_name, 
   char * ticket_type, char * ticket_zone);
/* Function for finding a specific ticket in the stock list.
 * Starts a loop at the head node of the stock list
 * If the passed in ticket name, type and zone all match those attributes in
 * the current node, this is the correct node and it's pointer is returned.
 * If the loop is finished without a matching node being found, no nodes exist
 * with the requested values and null is returned.*/

int remove_ticket(tm_type_ptr tm, char * ticket_name, char * ticket_type, 
   char * ticket_zone);
/* Function for removing a ticket node from the stock list.
 * Function operation is similar to find_ticket(), but removes the specified
 * ticket instead of returning it.
 * Starts the loop at the head node of the stock list. This loop keeps track
 * of the previous node in the list as well as the current one.
 * If the passed ticket name, type and zone all match the current node, this
 * is the requested ticket. If the previous node is null, the correct node is
 * the head node, so the next node in the list should become the new head node.
 * Otherwise, the previous node's pointer to the next node is reassigned to
 * equal the next node pointer of the current node, removing the current node
 * from the list. In both cases, the memory of the current node is then freed
 * and the number of stock items in the list is decreased by one. True is then
 * returned from the function.
 * If a node with the correct name, type and zone cannot be found in the list,
 * False is returned from the function.*/ 

void resupply_tickets(tm_type_ptr tm);
/* Function for returning all tickets to their default stock level.
 * Starts loop at the head node of the stock list. Goes through every node
 * on the list, and sets that node's ticket stock level to equal the default
 * constant defined in tm.h*/
#endif

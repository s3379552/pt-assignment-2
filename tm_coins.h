/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#ifndef TM_COINS_H
#define TM_COINS_H
#include "tm.h"

/* specifies the various coin types in the system and their values */
enum coin_types
{
    FIVE_CENTS=5,
    TEN_CENTS=10,
    TWENTY_CENTS=20,
    FIFTY_CENTS=50,
    ONE_DOLLAR=100,
    TWO_DOLLARS=200
};

/* defines a 'coin' in the system. the coinslist is actually an array of 
 * this type
 */
struct coin
{
    enum coin_types denomination;
    unsigned count;
};

void coins_init(tm_type_ptr tm);
/* Function for initialising the coins array.
 * Creates a pointer for the coins list, allocating enough memory for the
 * 6 coins that will be stored in the array.
 * It then sets each element of the array to equal one of the coins in
 * ascending order.
 * Finally, it loops through the array and sets all coins to have the
 * default stock level. It also assigns this coins array to the ticketing
 * machine passed into the function*/

int stock_coin(tm_type_ptr tm, int coin_val, unsigned coin_stock);
/* Function for restocking a coin with the specified stock level 
 * Loops through the coins array until it finds the matching coin to the one
 * passed into the function. This coins stock is then reassigned to be the
 * specified level. Finally, True is returned.
 * If the specified coin cannot be found in the array, False is returned.*/

void resupply_coins(tm_type_ptr tm);
/* Function for resupplying all coins in the array to the default stock level
 * Loops through the coins array and sets all stock levels to be the default
 * costant defined in tm.h*/

void add_coin(tm_type_ptr tm, int coin_val, unsigned added_stock);
/* Function for increasing a coin's stock level.
 * Loops through coins array until it finds the matching coin to the one
 * passed into the function. It then adds the value of added_stock to the 
 * coin's current stock level */

int remove_coin(tm_type_ptr tm, int coin_val, unsigned removed_stock);
/* Function for decreasing a coin's stock level.
 * Loops through coins array until it finds the matching coin to the one
 * passed into the function. It then decreases the coin's stock level by the
 * amount passed into the function, removed_stock, then return True. 
 * If this removal would bring the coin's stock level below zero, False is 
 * instead returned*/
#endif

/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/#include "tm_utility.h"

/* add any 'helper' functions here that provide generic service to your 
 * application. read_rest_of_line() is provided here as an example of this
 */
int ctrlDFlag = FALSE;
int read_int_input(unsigned *outputInt, int input_size, int max_int)
{
   char tempInput[NUM_ARGS + EXTRA_SPACES];
   int finished = FALSE;
   int tempInt = 0;
   char *endPtr;

   /* loop for accepting valid int input */
   do
   {
      /* reading character input from user, ctrl+D checking */
      if(fgets(tempInput, input_size + EXTRA_SPACES, stdin) == NULL){
         printf("\n");
         ctrlDFlag = TRUE;
         memset(tempInput, 0, input_size + EXTRA_SPACES);
      }
      
      /* input character length checking */
      if(tempInput[strlen(tempInput) - 1] != '\n' && tempInput[0] != '\0')
      {
         printf("Input is too long\n");
         read_rest_of_line();
      }
      
      /* empty line checking */
      else if(empty_line_check_char(tempInput))
      {
         return FALSE;
      }

      /* converting char input to integer input */
      else
      {
         /* checking that input string is all integers */
         tempInput[strlen(tempInput) - 1] = '\0';
         tempInt = (int) strtol(tempInput, &endPtr, 10);
         if (strcmp(endPtr, "") != 0)
         {
            printf("Please enter integers only.\n");
         }  
         
         /* integer size checking */
         else if(max_int < tempInt)
         {
            printf("Given input is too large, please enter an integer ");
            printf("within the correct range.\n");
         }
         
         /* assigning integer to paramater pointer */
         else
         {
            *outputInt = tempInt;
            finished = TRUE;
         }
      }
   } while(!finished);
   return TRUE;
}

void read_char_input(char *outputString, int input_size)
{
   int finished = FALSE;
   char * tempString;
   tempString = malloc(sizeof(char) * (input_size + EXTRA_SPACES));
   assert(tempString != NULL);

   /* loop for accepting valid user input */
   do
   {
      /* accepting input and checking for ctr+D */
      if(fgets(tempString, input_size + EXTRA_SPACES, stdin) == NULL)
      {
         printf("\n");
         ctrlDFlag = TRUE;
         memset(tempString, 0, input_size + EXTRA_SPACES);
      }
      
      /* checking input length */
      if (tempString[strlen(tempString) - 1] != '\n' && tempString[0] != '\0')
      {
         printf("Input is too long. Please enter an input within the ");
         printf("correct range.\n");
         read_rest_of_line();
      }
      else
      {
         tempString[strlen(tempString) - 1] = '\0';
         finished = TRUE;
      }
   } while(!finished); 

   /* copy user string from temp string to paramater string */
   strcpy(outputString, tempString);
   free(tempString);
}

/* Function for checking if the user has input an empty line */
int empty_line_check_char(char * input)
{
   if(input[0] == '\0' || ctrlDFlag == TRUE)
   {
      printf("Empty string detected. Returning to main menu.\n");
      ctrlDFlag = FALSE;
      return TRUE;
   }
   return FALSE;
}

/* Function for checking if the user has input ASCII characters */
int ascii_check(char * input)
{
   int i;

   /* Looping through input string */
   for (i = 0; strlen(input); i++)
   {
      /* checking for end of string */
      if(input[i] == '\0')
      {
         return TRUE;
      }

      /* checking for invalid character input */
      if(input[i] < 32 || input[i] > 126)
      {
         printf("Please enter non-blank ASCII characters only.\n");
         return FALSE;
      }
   }

   return FALSE;
}

/* Function for getting ticket type, zone and name from user */
int get_ticket_input(char * ticket_name, char * ticket_type, 
   char * ticket_zone)
{
 
   /* Ticket Type input */
   while (1)
   {
      printf("Enter a ticket type ('F' for Full Fare, 'C' for Concession): ");
      read_char_input(ticket_type, TICKET_TYPE_LENGTH);

      /* empty line checking */
      if(empty_line_check_char(ticket_type) == TRUE)
      {
         printf("Empty input detected. Returning to menu.\n");
         return FALSE;
      }
      
      /* character range checking */
      if(*ticket_type == 'F' || *ticket_type == 'C')
      {
         break;
      }
      printf("Please enter a valid ticket zone\n");
   }

   /* Ticket zone input */
   while (1)
   {
      printf("Enter a zone (1, 2 or 1+2): ");
      read_char_input(ticket_zone, TICKET_ZONE_LENGTH);

      /* empty line checking */
      if(empty_line_check_char(ticket_zone) == TRUE)
      {
         printf("Empty input detected. Returning to menu.\n");
         return FALSE;
      }

      /* character range checking */
      if(strcmp(ticket_zone, "1") == 0 || strcmp(ticket_zone, "2") ==  0 ||
         strcmp(ticket_zone, "1+2") == 0)
      {
         break;
      }
      printf("Please enter a valid ticket type.\n");
   }

   /* Ticket name input */
   while (1)
   {
      printf("Enter a ticket name (1-40 characters): ");
      read_char_input(ticket_name, TICKET_NAME_LEN);

      /* empty line checking */
      if(empty_line_check_char(ticket_name) == TRUE)
      {
         printf("Empty input detected. Returning to menu.\n");
         return FALSE;
      }
      
      /* range checking */
      if(ascii_check(ticket_name) == TRUE)
      {
         break;
      }
   }
   return TRUE;
}
/***************************************************************************
* read_rest_of_line() - reads characters from the input buffer until 
* all characters have been cleared. You will need to call this in 
* association with fgets().
***************************************************************************/
void read_rest_of_line(void)
{
    int ch;
    while(ch=getc(stdin), ch!=EOF && ch!='\n') 
        ;
    clearerr(stdin);
}

/***************************************************************************
* system_init() - allocate memory if necessary and initialise the struct tm
* to safe initial values.
***************************************************************************/
BOOLEAN system_init(tm_type * tm)
{
   coins_init(tm);
   /*initalise structure linked list of stock*/
   stock_init(tm);
   return TRUE;
   /* if something goes wrong, return FALSE;*/
}

/***************************************************************************
* load_data() - load data from both the stock file and the coins file and 
* populate the datastructures keeping in mind data validation and sorting 
* requirements. This function implements the requirement 2 from the 
* assignment 2 specifications.
***************************************************************************/
BOOLEAN load_data(tm_type * tm, char * stockfile, char * coinsfile)
{
   char * line_token;
   char *attr_token;
   int coin_val;
   unsigned int coin_stock;
   char *ticket_name;
   char ticket_type;
   char *ticket_zone;
   unsigned int ticket_price;
   unsigned int ticket_stock;
   char attr_delim[2] = ",";

   /*coins loading*/
   line_token = strchr(coinsfile, '\n');
   while(line_token != NULL)
   {
      /* replace newline character with EOF and move to next character */
      *line_token++ = '\0';

      /* get next line of file data */
      attr_token = strtok(coinsfile, attr_delim);

      /* first token is for the coin value */
      if(attr_token == NULL)
      {
         /*error code*/
         fprintf(stderr, "Coin value not found.\n"); 
         return FALSE;
      }
      /* convert token to integer */
      coin_val = atoi(attr_token);
      
      /* integer data type checking */
      if(coin_val == 0)
      {
         fprintf(stderr, "Coin value not an integer.\n");
         return FALSE;
      }

      /* second token is for the coin qty */
      attr_token = strtok(NULL, attr_delim);
      if(attr_token == NULL)
      {
         fprintf(stderr, "Coin stock not found.\n");
         return FALSE;
      }
      coin_stock = (unsigned int)atoi(attr_token);
      if(coin_stock == 0)
      {
         fprintf(stderr, "Coin stock not an integer.\n");
         return FALSE;
      }
      attr_token = strtok(NULL, attr_delim);
      if(attr_token != NULL)
      {
         fprintf(stderr, "Extra (unnecessary) coin information found.\n");
         return FALSE;
      }
      
      /*insert coin stock into array*/
      if(!stock_coin(tm, coin_val, coin_stock))
      {
         fprintf(stderr, "Coin not of correct denomination.\n");
         return FALSE;
      }
      /* move to next line of file (from current line to next new line */
      coinsfile = line_token;
      line_token = strchr(coinsfile, '\n');
   }
   
   /*stock loading*/
   /*uses same process as coins loading */
   line_token = strchr(stockfile, '\n');
   while(line_token != NULL)
   {
      *line_token++ = '\0';
      attr_token = strtok(stockfile, attr_delim);
      if(attr_token == NULL)
      {
         fprintf(stderr, "Ticket name not found.\n"); 
         return FALSE;
      }
      ticket_name = attr_token;
      attr_token = strtok(NULL, attr_delim);
      if(attr_token == NULL)
      {
         fprintf(stderr, "Ticket type not found.\n"); 
         return FALSE;
      }
      ticket_type = *attr_token;

      /* character range checking */
      if(ticket_type != 'C' && ticket_type != 'F')
      {
         fprintf(stderr, "Ticket type not valid type.\n");
         return FALSE;
      }
      attr_token = strtok(NULL, attr_delim);
      if(attr_token == NULL)
      {
         fprintf(stderr, "Ticket zone not found.\n"); 
         return FALSE;
      }
      if(strcmp(attr_token, "1+2") != 0 && strcmp(attr_token, "1") != 0 &&
         strcmp(attr_token, "2") != 0)
      {
         fprintf(stderr, "Ticket zone not valid value.\n");
         return FALSE;
      }
      ticket_zone = attr_token;
      attr_token = strtok(NULL, attr_delim);
      if(attr_token == NULL)
      {
         fprintf(stderr, "Ticket price not found.\n"); 
         return FALSE;
      }
      ticket_price = (unsigned int)atoi(attr_token);
      if(ticket_price == 0)
      {
         fprintf(stderr, "Ticket price not an integer.\n");
         return FALSE;
      }
      attr_token = strtok(NULL, attr_delim);
      if(attr_token == NULL)
      {
         fprintf(stderr, "Ticket stock level not found.\n"); 
         return FALSE;
      }
      ticket_stock = (unsigned int)atoi(attr_token);
      if(ticket_stock == 0)
      {
         fprintf(stderr, "Ticket stock level not an integer.\n");
         return FALSE;
      }
      attr_token = strtok(NULL, attr_delim);
      if(attr_token != NULL)
      {
         fprintf(stderr, "Extra (unnecessary) ticket information found.\n");
         return FALSE;
      } 
      /*insert ticket*/
      new_ticket(tm, ticket_name, ticket_type, ticket_zone, ticket_price,
         ticket_stock); 
      stockfile = line_token;
      line_token = strchr(stockfile, '\n');
   }
   return TRUE;
}

/**************************************************************************
 * system_free() - free all memory allocated for the tm datastructure.
 *************************************************************************/
void system_free(tm_type_ptr tm)
{
   struct stock_node * current_node = tm->stock->head_stock;
   struct stock_node * previous_node = NULL;
   
   /* loop for freeing stock node data and node itself */
   while(current_node != NULL)
   {
      previous_node = current_node;
      current_node = current_node->next_node;
      free(previous_node->data);
      free(previous_node);
   }
   
   /* freeing stock list */
   free(tm->stock);
   
   /* freeing coins array */
   free(tm->coins); 
}


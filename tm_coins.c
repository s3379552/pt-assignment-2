/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm_coins.h"

/* add functions here for the manipulation of the coins list (array) */

/* Function for initialising the coins array */
void coins_init(tm_type * tm)
   {  
      int i;
      
      /* Creates a pointer for the coin array, allocates its memory */
      struct coin * coin_list = malloc(sizeof(struct coin) * NUM_COINS);
      assert(coin_list != NULL);

      /* Addes all 6 coins to the array */
      coin_list[0].denomination = FIVE_CENTS;
      coin_list[1].denomination = TEN_CENTS;
      coin_list[2].denomination = TWENTY_CENTS;
      coin_list[3].denomination = FIFTY_CENTS;
      coin_list[4].denomination = ONE_DOLLAR;
      coin_list[5].denomination = TWO_DOLLARS;

      /* Sets all coins stock level to the default level */
      for(i = 0; i < NUM_COINS; i++)
      {
         coin_list[i].count = DEFAULT_COINS_COUNT;
      }
      
      /* Assign the created array to the ticketing machine */
      tm->coins = coin_list;
   }

/* Function for changing a coin's stock level to the specified level */
int stock_coin(tm_type * tm, int coin_val, unsigned coin_stock)
{
   int i;
   
   /* Loops through coins array to find specified coin */
   for(i =0; i < NUM_COINS; i++)
   {
      if(tm->coins[i].denomination == coin_val)
      {
         /* Once specified coin is found, set its stock level to the specified
 *          value and return true*/
         tm->coins[i].count = coin_stock;
         return TRUE;
      }
   }
   
   /* If coin is not found, return false */
   return FALSE;
}

/* Function for resupplying all coins to default level */
void resupply_coins(tm_type * tm)
{
   int i;
   /* Loop through coins array and change all stock levels to default value */
   for(i = 0; i < NUM_COINS; i++)
   {
      tm->coins[i].count = DEFAULT_COINS_COUNT;
   }
   return;
}

/* Function for adding more stock to a coin's current level */
void add_coin(tm_type_ptr tm, int coin_val, unsigned added_stock)
{
   int i;
   
   /* Loop through array until correct coin is found*/
   for(i =0; i < NUM_COINS; i++)
   {
      if(tm->coins[i].denomination == coin_val)
      {
         /* Add added_stock value to current stock value */
         tm->coins[i].count += added_stock;
         return;
      }
   }
}

/* Function for removing stock from a coin's current level */
int remove_coin(tm_type_ptr tm, int coin_val, unsigned removed_stock)
{
   int i;

   /* Loop through array until correct coin is found */
   for(i =0; i < NUM_COINS; i++)
   {
      if(tm->coins[i].denomination == coin_val)
      {
         /* If removing the requested stock level would bring stock level
 *          below 0, return False */
         if((tm->coins[i].count - removed_stock) >= 0)
         {
            /* Otherwise, remove specified stock level */
            tm->coins[i].count -= removed_stock;
            return TRUE;
         }
         return FALSE;
      }
   }
   /* If coin cannot be found, return False */
   return FALSE;

}


###############################################################################
# COSC1283/1284 - Programming Techniques
# Semester 2 2013 Assignment #2 - SCSIT Program Management System
# Full Name        :	Adam Thompson
# Student Number   : s3379552
# Yallara Username : s3379552
# Course Code      : COSC1284
# Program Code     : BP094SEC8
# Start up code provided by Paul Miller and Lin Padgham
################################################################################

.PHONY:archive
archive:
	zip $(USER) tm.c tm.h tm_coins.c tm_coins.h tm_options.c tm_options.h \
	tm_stock.c tm_stock.h tm_utility.c tm_utility.h tm_readme.txt Makefile
clean:
	rm -f tm *.o
all: tmMake
	
utility.o: tm_utility.c tm_utility.h
	gcc -ansi -Wall -pedantic -c tm_utility.c -o utility.o
stock.o: tm_stock.c tm_stock.h
	gcc -ansi -Wall -pedantic -c tm_stock.c -o stock.o
options.o: tm_options.c tm_options.h
	gcc -ansi -Wall -pedantic -c tm_options.c -o options.o
coins.o: tm_coins.c tm_coins.h
	gcc -ansi -Wall -pedantic -c tm_coins.c -o coins.o
tm.o: tm.c tm.h
	gcc -ansi -Wall -pedantic -c tm.c -o tm.o
tmMake: utility.o stock.o options.o coins.o tm.o
	gcc utility.o stock.o options.o coins.o tm.o -o tm

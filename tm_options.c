/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/#include "tm_options.h"

/* add functions here for the implementation of each of the options
 * required to fulfil the requirements of the assignment
 */

/**************************************************************************
* purchase_ticket() - get input from the user about the ticket they wish 
* to purchase, retrieve that ticket from the list and adjust levels of the
* stock and the coins upon payment of the ticket. This function implements
* requirement 4 from the assignment 2 specification.
**************************************************************************/
void purchase_ticket(tm_type * tm)
{
   /*int ticket_return;*/
   char ticket_name [TICKET_NAME_LEN+EXTRA_SPACES];
   char ticket_type;
   char * ticket_type_ptr;
   char ticket_zone [TICKET_ZONE_LENGTH+EXTRA_SPACES];
   struct stock_data * ticket_data;
   unsigned entered_coins = 0;
   unsigned ticket_cost;
   unsigned current_coin;
   int change_needed=0;
   unsigned coin_2_count=0;
   unsigned coin_1_count=0;
   unsigned coin_5c_count=0;
   unsigned coin_10c_count=0;
   unsigned coin_20c_count=0;
   unsigned coin_50c_count=0;
   unsigned coin_2_change=0;
   unsigned coin_1_change=0;
   unsigned coin_5c_change=0;
   unsigned coin_10c_change=0;
   unsigned coin_20c_change=0;
   unsigned coin_50c_change=0;

   ticket_type_ptr = &ticket_type;
   printf("Purchase A Ticket\n");
   printf("-----------------\n");

   /* get ticket name, type and zone */
   if(!(get_ticket_input(ticket_name, ticket_type_ptr, ticket_zone)))
      return;

   /* find the ticket specified in the list */
   ticket_data = find_ticket(tm, ticket_name, ticket_type_ptr, ticket_zone);

   /* check for ticket existance */
   if(ticket_data == NULL)
   {
      printf("Ticket not found. Returning to menu.\n");
      return;
   }

   /* check for ticket stock level */
   if(ticket_data->stock_level == 0)
   {
      printf("Ticket cannot be bought, as it's stock is currently at zero.\n");
      return;
   }
   ticket_cost = ticket_data->ticket_price;
   printf("The price is $%.2f\n", COIN_CONVERT(ticket_cost));

   /* Loop for asking for coin payment */
   while(ticket_cost > entered_coins)
   {
      printf("Enter a coin ($%.2f remaining): ", 
         COIN_CONVERT(ticket_cost - entered_coins));

      /* if input isn't valid restart loop */
      if(!read_int_input(&current_coin, 3, TWO_DOLLARS))
         return;

      /* loop for incrementing correct coin value */
      if(current_coin == TWO_DOLLARS)
         coin_2_count++;
      else if(current_coin == ONE_DOLLAR)
         coin_1_count++;
      else if(current_coin == FIVE_CENTS)
         coin_5c_count++;
      else if(current_coin == TEN_CENTS)
         coin_10c_count++;
      else if(current_coin == TWENTY_CENTS)
         coin_20c_count++;
      else if(current_coin == FIFTY_CENTS)
         coin_50c_count++;
      else
      {
         printf("Please enter a valid coin value.\n");
         continue;
      }
      entered_coins += current_coin;
   }

   /* add user input coins to coins array quantities */
   add_coin(tm, TWO_DOLLARS, coin_2_count);
   add_coin(tm, ONE_DOLLAR, coin_1_count);
   add_coin(tm, FIVE_CENTS, coin_5c_count);
   add_coin(tm, TEN_CENTS, coin_10c_count);
   add_coin(tm, TWENTY_CENTS, coin_20c_count);
   add_coin(tm, FIFTY_CENTS, coin_50c_count);
   change_needed = entered_coins - ticket_cost;
   printf("Thank you for purchasing a %s ", ticket_data->ticket_name);
   if(ticket_data->ticket_type == 'C')
      printf("concession ");
   else
      printf("full fare ");
   printf("zone %s ticket.\n", ticket_data->ticket_zone);

   /* Purchase ticket if no change needed */
   if(change_needed == 0)
   {
      printf("No change needed.\n");
      ticket_data->stock_level--;
      return;
   }
   printf("Your change is: $%.2f\n", COIN_CONVERT(change_needed));
   printf("Your change coins are: ");
   /* loop for giving change */
   while(change_needed > 0)
   {
      /* checking if coin can be given as change */
      if((change_needed - TWO_DOLLARS) >= 0)
      {
         /* checking if removing a single coin is successful */
         if(remove_coin(tm, TWO_DOLLARS, 1))
         {
            /* printing another coin */
            coin_2_change++;
            printf("$2 ");
            change_needed -= 200;
         }
      }
      else if((change_needed - ONE_DOLLAR) >= 0)
      {
         if(remove_coin(tm, ONE_DOLLAR, 1))
         {
            coin_1_change++;
            printf("$1 ");
            change_needed -= 100;
         }
      }
      else if((change_needed - FIFTY_CENTS) >= 0)
      {
         if(remove_coin(tm, FIFTY_CENTS, 1))
         {
            coin_50c_change++;
            printf("50c ");
            change_needed -= 50;
         }
      }
      else if((change_needed - TWENTY_CENTS) >= 0)
      {
         if(remove_coin(tm, TWENTY_CENTS, 1))
         {
            coin_20c_change++;
            printf("20c ");
            change_needed -= 20;
         }
      }
      else if((change_needed - TEN_CENTS) >= 0)
      {
         if(remove_coin(tm, TEN_CENTS, 1))
         {
            coin_10c_change++;
            printf("10c ");
            change_needed -= 10;
         }
      }
      else if((change_needed - FIVE_CENTS) >= 0)
      {
         if(remove_coin(tm, FIVE_CENTS, 1))
         {
            coin_5c_change++;
            printf("10c ");
            change_needed -= 5;
         }
      }

      /* if change cannot be given */
      else
      {
         printf(" ERROR.\n");
         printf("Ticket Machine cannot provide the correct amount of change.\n");
         printf("All deposited coins refunded and change not dispensed.\n");

         /* give back all coins input by user */
         remove_coin(tm, TWO_DOLLARS, coin_2_count);
         remove_coin(tm, ONE_DOLLAR, coin_1_count); 
         remove_coin(tm, FIFTY_CENTS, coin_50c_count);
         remove_coin(tm, TWENTY_CENTS, coin_20c_count);
         remove_coin(tm, TEN_CENTS, coin_10c_count);
         remove_coin(tm, FIVE_CENTS, coin_5c_count);

         /* return all coins previously given out as change */
         add_coin(tm, TWO_DOLLARS, coin_2_change);
         add_coin(tm, ONE_DOLLAR, coin_1_change);
         add_coin(tm, FIVE_CENTS, coin_5c_change);
         add_coin(tm, TEN_CENTS, coin_10c_change);
         add_coin(tm, TWENTY_CENTS, coin_20c_change);
         add_coin(tm, FIFTY_CENTS, coin_50c_change);
         return;
      }
   }
   ticket_data->stock_level--; 
   printf("\n");
}

/**************************************************************************
* display_tickets() - iterate over the stock list and display a list of 
* tickets in the system according to the format specified in the assignment
* 2 specifications. This option implements requirement 5 from the assignment
* specifications.
**************************************************************************/
void display_tickets(tm_type * tm)
{
   float coin;
   char *full_ticket_type;
   struct stock_node * current_node = tm->stock->head_stock;
   printf("Ticket          Type          Zone       Price  Qty\n");
   printf("--------------- ------------- ---------- ------ ---\n");

   /* Looping through stock list */
   while(current_node != NULL)
   {
      
      /* Checking ticket type */   
      if(current_node->data->ticket_type == 'C')
      {
         full_ticket_type = "Concession";
      }
      else if(current_node->data->ticket_type == 'F')
      {
         full_ticket_type = "Full Fare";
      }

      /* Printing current ticket node */
      coin = (float)(COIN_CONVERT(current_node->data->ticket_price));
      printf("%-15s %-13s %-10s $%-5.2f %3d\n", 
         current_node->data->ticket_name, full_ticket_type, 
         current_node->data->ticket_zone, coin, 
         current_node->data->stock_level);
      current_node = current_node->next_node;
   }
}

/***************************************************************************
* add_ticket() - Request user input about creating a new ticket type in 
* the system. You need to validate this input and then create and insert 
* a new ticket into the system, sorted by ticket name. this option 
* implements requirement 6 in the assignment 2 specifications.
**************************************************************************/
void add_ticket(tm_type * tm)
{
   char ticket_name [TICKET_NAME_LEN+EXTRA_SPACES];
   char ticket_type;
   char * ticket_type_ptr;
   char ticket_zone [TICKET_ZONE_LENGTH+EXTRA_SPACES];
   unsigned ticket_price;
   ticket_type_ptr = &ticket_type;
   printf("Add Ticket\n");
   printf("----------\n");

   /* Getting new ticket name type and zone from user */
   if(!(get_ticket_input(ticket_name, ticket_type_ptr, ticket_zone)))
      return;

   /* Getting new ticket price */
   printf("Price (in cents. Max $50): ");
   if(!read_int_input(&ticket_price, 4, MAX_TICKET_COST))
   {
      return;
   }
   
   /* Adding new ticket to list */
   new_ticket(tm, ticket_name, ticket_type, ticket_zone, ticket_price,
      DEFAULT_STOCK_LEVEL);
   
}

/**************************************************************************
* delete_ticket() - Request user input from the administrator specifying
* the name, type and zone of the ticket. You will then search the stock
* list to delete this ticket. All three fields entered must match for the
* ticket to be deleted. Please ensure that you free any memory that was 
* dynamically allocated. This function implements requirement 7 from the 
* assignment 2 specifications.
**************************************************************************/
void delete_ticket(tm_type * tm)
{
   char ticket_name [TICKET_NAME_LEN+EXTRA_SPACES];
   char ticket_type;
   char * ticket_type_ptr;
   char ticket_zone [TICKET_ZONE_LENGTH+EXTRA_SPACES];
   ticket_type_ptr = &ticket_type;
   printf("Delete Ticket\n");
   printf("-------------\n");

   /* Getting requested name, type and zone of ticket */
   if(!(get_ticket_input(ticket_name, ticket_type_ptr, ticket_zone)))
      return;

   /* Removing ticket/checking for existance */
   if(!(remove_ticket(tm, ticket_name, ticket_type_ptr, ticket_zone)))
   {  
      printf("Ticket to remove cannot be found.\n");
      return;
   }

}

/**************************************************************************
* display_coins() - display a list of each of the coin types in the system
* and the count of each coin type according to the format specified in the
* assignment 2 specification. This function implements requirement 8 from
* the assignment 2 specification.
**************************************************************************/
void display_coins(tm_type * tm)
{
   int i;
   float sum;
   float coin;
   float coin_total = 0;
   int qty;
   printf("Coin  Quantity Value   \n");
   printf("----- -------- --------\n");

   /* Printing out each coin in array */
   for(i = 0; i < NUM_COINS; i++)
   {
      coin = (float)(COIN_CONVERT(tm->coins[i].denomination));
      qty = tm->coins[i].count;
      coin_total = coin * (float)(qty);
      printf("$%1.2f %8d $%.2f\n", coin, qty, coin_total);
      sum += coin_total;
   }
   printf("               --------\n");

   /* Printing out total ticket machine coin value */
   printf("Total Value:   $%.2f\n", sum);
}

/**************************************************************************
* restock_tickets() - iterate over the list and set all ticket products
* to the default level specified in tm.h. This function implements the 
* requirement 9 in the assignment 2 specifications.
**************************************************************************/
void restock_tickets(tm_type * tm)
{
   printf("Restock Tickets\n");
   printf("---------------\n");

   resupply_tickets(tm);

   printf("All ticket products have been restocked to the default level\n");
}

/**************************************************************************
* restock_coins() - set the total number of coins of each denomination to
* the default number specified in tm.h. This function implements the 
* requirement 10 from the assignment 2 specifications.
**************************************************************************/
void restock_coins(tm_type * tm)
{
   printf("Restock Coins\n");
   printf("-------------\n");

   resupply_coins(tm);

   printf("All coins have been restocked to the default level.\n");
}

/**************************************************************************
* save_data() - open the stockfile and the coinsfile for writing and 
* save all the data to these files. You need to ensure that you save data
* to these files in the same format as specified for loading this data in 
* so that your program or any alternate but correct implementation could
* load the resultant data files. This function implements requirement 11
* from the assignment 2 specifications.
**************************************************************************/
void save_data(tm_type * tm, char * stockfile, char * coinsfile)
{
   FILE * coin_save_FP;
   FILE * stock_save_FP;
   char * element_comma = ",";
   char * new_line = "\n";
   struct stock_node * current_node = tm->stock->head_stock;
   int i; 
   coin_save_FP = fopen(coinsfile, "w");
   stock_save_FP = fopen(stockfile, "w");

   /* writing to stock file*/
   while (current_node != NULL)
   {
      fwrite(current_node->data->ticket_name, 1, 
         strlen(current_node->data->ticket_name), stock_save_FP);
      fputs(element_comma, stock_save_FP);
      fwrite(&current_node->data->ticket_type, 1, 1, stock_save_FP);
      fputs(element_comma, stock_save_FP);
      fwrite(current_node->data->ticket_zone, 1, 
         strlen(current_node->data->ticket_zone), stock_save_FP);
      fputs(element_comma, stock_save_FP);
      fprintf(stock_save_FP, "%d", current_node->data->ticket_price);
      fputs(element_comma, stock_save_FP);
      fprintf(stock_save_FP, "%d", current_node->data->stock_level);

      /* Checking if another line in file is needed */
      if(current_node->next_node != NULL)
      {
         fputs(new_line, stock_save_FP);
      }
      current_node = current_node->next_node;
   }

   /* writing to coins file */
   for(i = 0; i < NUM_COINS; i++)
   {
      fprintf(coin_save_FP, "%d,%d", tm->coins[i].denomination, 
         tm->coins[i].count);
      
      /* Checking if another line in file is needed */
      if(i < (NUM_COINS - 1))
         fprintf(coin_save_FP, "\n");
   }

   /* Close file pointers */
   fclose(stock_save_FP);
   fclose(coin_save_FP);
}

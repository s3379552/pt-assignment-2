/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm.h"
#ifndef TM_UTILITY_H
#define TM_UTILITY_H

/* function prototypes for functions implemented in tm_utility.c
 * These are generic helper functions that provide support to the rest
 * of your application.
 *
 * Remember that we will assess the level to which you provide some level
 * of functional abstraction here.
 */
int empty_line_check_char(char * input);
/* Function for checking if the user has input an empty line.
 * If the first character of the input string is EOF, or if the global
 * flag for a ctrl+D input is set, then an empty line has occured.
 * If this is the case, reset the ctrl+D flag and return True.
 * If neither of these instances have occured, then return False.*/

int ascii_check(char * input);
/* Function for checking if the user entered a valid range of characters.
 * Loops through the input string. If an EOF character is found, all 
 * previous characters are valid characters and return TRUE.
 * If a character is encountered that is not within the valid range,
 * return FALSE. */

int get_ticket_input(char * ticket_name, char * ticket_type,
   char * ticket_zone); 
/* Function for getting a ticket type, zone and name from the user.
 * These inputs are saved to the parameters passed to the function.
 * The user is first asked for a ticket zone. This input is then checked
 * for both empty line cases and that it is a valid value. If an empty line
 * is detected, then False is returned. If the input is not a valid type, a
 * new type is requested from the user. This process is repeated for ticket 
 * zone and ticket name (ticket name does not have a type check as any ASCII
 * characters are allowed). Once all 3 inputs have been satisfied, True is
 * returned. */

void read_char_input(char * outputString, int input_size);
/* Function for reading ASCII character input from the user.
 * A temporary storage array for the input is created and its memory is
 * allocated. Input is then requested from the user. If fgets returns null,
 * then ctrl+D was input and the ctrl+D flag is set. The memory of the temp
 * storage is also cleared. If the the 2nd last character of the temp storage
 * is not a newline character and the first character is not an EOF (not ctrl+D
 * input), then the input given is too long and the buffer is cleared. If these
 * cases are not apparent, then the input is valid and the newline character is
 * replaced with an EOF character, and the loop for asking for input is broken.
 * The value of this temporary storage (the user string) is then copied to the
 * string passed to the function, and the memory of the temporary storage is
 * freed */

int read_int_input(unsigned *outputInt, int input_size, int max_int);
/* Function for reading integer input from the user. 
 * The beginning of this function is very similar to the read_char_input 
 * function. This function also checks the input string for empty line
 * input using empty_line_check_char(). If this function returns true,
 * then False is returned. Otherwise, the newline character is replaced with
 * EOF, and the input string is converted to a decimal integer type.
 * This conversion is checked that is in the requested integer range,
 * and if it is, the value is copied into the integer pointer passed to the
 * function, and the input loop is broken. If any checks are failed, a new
 * input is requested. */

void read_rest_of_line(void);
/* Function for clearing the input buffer */

BOOLEAN system_init(tm_type_ptr tm);
/* Function for initialising a ticket machine.
 * The ticket machine pointer is passed to the coins and stock initialiser
 * functions, which initialise their respective parts of the ticketing
 * machine. Since the assert's used in each of these functions immediately
 * abort the program, and initialisation failures are not reutnred from the
 * functions, so if these functions return at all, the initialisation was
 * successful and True is returned. */

BOOLEAN load_data(tm_type_ptr tm, char * stockfile, char * coinsfile);
/* Function for loading command line data into the ticketing machine.
 * NOTE: Due on part to me having a senior moment when making this function,
 * the passed char pointers are arrays with the entirety of each file in them,
 * not the name of the file itself. It still works, just an odd implementation.
 *
 * Each file (first coins file, then stock file) is loaded one after the other,
 * using the same method of reading the data. 
 * The first instance of a newline character is found in the file data, and
 * this position is then replaced with an EOF character. A string token is
 * generated from the file data, which will read from the current position up
 * to the EOF which replaced the previous newline character. This string is
 * tokenised with strtok into its seperate elements. As each element token is
 * created, the token is checked for existance and correct data type. If any
 * token is missing, an incorrect data type or there is more tokens than
 * expected, the file is incorrect and False is returned from the function.
 * If all tokens are correct, these tokens are passed into the file's
 * respective function for creating a new entry in the ticketing machine.
 * If this function returns False, then the entry creation has failed and
 * False is returned. Finally, The next newline character is found in the file
 * and the tokenising loop starts again. This loop is done for the coins file
 * and then the stock file. If both of these loops complete, then True is 
 * returned. */
void system_free(tm_type_ptr);
/* Function for freeing all dynamically allocated memory from the ticketing 
 * machine.
 * First, the stock list is freed. The list is looped over, keeping track of
 * the previous node and the current node. Each time the previous node's data
 * then the node itself is freed. Finally, the stock list itself is freed.
 * For the coins array, the coins array pointer is freed, which cointains all
 * the coin data. */
#endif

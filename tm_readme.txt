/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/

If selected, do you grant permission for your assignment
to be released as an anonymous student sample solution?
--------------------------------------------------------
Yes


Known bugs:
-----------



Incomplete functionality:
-------------------------



Assumptions:
------------
A new ticket's price cannot exceed $50


Any other notes for the marker:
-------------------------------
Not a bug that appears in the final program, but if the
reading ticket input function asked for the ticket name first, the ticket name
would not be retained after asking for the second input. As such, the ticket
name is now the last thing requested, and it is now retained.
